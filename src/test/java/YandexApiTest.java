import entity.YandexResponse;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import org.junit.jupiter.api.Test;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

public class YandexApiTest {

    private final String BASE_URL = "https://api.weather.yandex.ru/v1/forecast/";
    private final String KEY = "6438936d-3f05-41ec-9cfd-115262532ea5";

    HttpLoggingInterceptor logging = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);

    OkHttpClient.Builder httpClient = new OkHttpClient.Builder().addInterceptor(logging);

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(httpClient.build())
            .build();
    @Test
    public void yandexApiTest() throws IOException {

        YandexApiClient yandexApiClient = retrofit.create(YandexApiClient.class);
        Call<YandexResponse> call = yandexApiClient.getResponse(KEY,  56.850573, 53.202836, 2);

        YandexResponse getApiResponse = call.execute().body();

        assertEquals(56.850573, getApiResponse.getInfo().getLat());
        assertEquals(53.202836,getApiResponse.getInfo().getLon());
        //по координатам не отдает имя часового пояса и его сокращенное название, ссылка типа https://yandex.ru/pogoda/?lat=56.850573&lon=53.202836 вместо https://yandex.ru/pogoda/moscow
//        assertEquals(10800, getApiResponse.getInfo().getOffset());
//        assertEquals("Europe/Moscow", getApiResponse.getInfo().getName());
//        assertEquals("MSK", getApiResponse.getInfo().getAbbr());
//        assertEquals(false, getApiResponse.getInfo().getDst());
//        assertEquals("https://yandex.ru/pogoda/moscow", getApiResponse.getInfo().getUrl());
    }
}
