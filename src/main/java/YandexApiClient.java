import entity.YandexResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

import java.util.List;

public interface YandexApiClient {
    @GET("https://api.weather.yandex.ru/v1/forecast")
    Call<YandexResponse> getResponse(@Header("X-Yandex-API-Key") String key,
                                           @Query("lat") Double lat,
                                           @Query("lon") Double lon,
                                           @Query("limit") int limit);
}
