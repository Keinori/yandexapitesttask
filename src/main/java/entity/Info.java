package entity;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Info {
   private Double lat;
   private Double lon;
   private Tzinfo tzinfo;
   private double offset;
   private String name;
   private String abbr;
   private Boolean dst;
   private Double def_pressure_mm;
   private Double def_pressure_pa;
   private String url;
}
