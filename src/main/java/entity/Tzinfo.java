package entity;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter

public class Tzinfo {
    private Double offset;
    private String name;
    private String abbr;
    private Boolean dst;
}
