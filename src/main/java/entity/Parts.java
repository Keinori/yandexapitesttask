package entity;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter

public class Parts {
    private Night night;
    private Morning morning;
    private Day day;
    private Evening evening;
    private Day_short day_short;
    private Night_short night_short;
}
