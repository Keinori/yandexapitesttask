package entity;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class YandexResponse {
    private Long now;
    private String now_dt;
    private List<Forecasts> forecasts;
    private Fact fact;
    private Info info;
}
