package entity;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Hours {
    private String hour;
    private Double hour_ts;
    private Double temp;
    private Double feels_like;
    private String icon;
    private String condition;
    private Double wind_speed;
    private Double wind_gust;
    private String wind_dir;
    private Double pressure_mm;
    private Double pressure_pa;
    private Double humidity;
    private Double prec_mm;
    private Double prec_period;
    private Double prec_type;
    private Double prec_strength;
    private Double cloudness;
}
