package entity;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class Forecasts {
    private String date;
    private Long date_ts;
    private Double week;
    private String sunrise;
    private String sunset;
    private Double moon_code;
    private String moon_text;
    private Parts parts;
    private Night night;
    private Integer temp_min;
    private Integer temp_max;
    private Integer temp_avg;
    private Integer feels_like;
    private String icon;
    private String condition;
    private String daytime;
    private Boolean polar;
    private Double wind_speed;
    private Double wind_gust;
    private String wind_dir;
    private Double pressure_mm;
    private Double pressure_pa;
    private Double humidity;
    private Double prec_mm;
    private Double prec_period;
    private Double prec_type;
    private Double prec_strength;
    private Double cloudness;
    private Day_short day_short;
    private Double temp;
    private List<Hours> hours;
    private String hour;
    private Double hour_ts;
}
