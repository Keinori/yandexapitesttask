package entity;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Fact {
    private Double temp;
    private Double feels_like;
    private Double temp_water;
    private String icon;
    private String condition;
    private Double wind_speed;
    private Double wind_gust;
    private String wind_dir;
    private Double pressure_mm;
    private Double pressure_pa;
    private Double humidity;
    private String daytime;
    private Boolean polar;
    private String season;
    private Double obs_time;
    private Double prec_type;
    private Double prec_strength;
    private Double cloudness;
}
